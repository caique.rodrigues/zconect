[1429] Lucas Mayworm Perrut
    
## O que este PR faz


- Disponibiliza o valor das transa��es em centavos para evitar problemas de arredondamento;
- Aumenta cobertura de testes.


## Como verificar se este PR est� correto


- Executar os testes de unidade e integra��o;
- Fazer um request para o endpoint v1transactions;
- Verificar se o valor est� em centavos;
- Verificar se o healthcheck retorna 200.


## Possui depend�ncias ou blockers


- [X] Sim
    - Depende do pull request #123 pois este adiciona a coluna valueInCents no barramento de servi�o.


## Checklist


- [ ] Vers�o atualizada corretamente nos arquivos de configura��o (AssemblyInfo.cs, change_request.yml e .nuspec);
- [ ] Documenta��o atualizada;
- [ ] Banco de dados de produ��o atualizado (se aplic�vel).









