﻿using Zurich.ZConect.Models;
using OpenQA.Selenium;
using TestUtils;

namespace Zurich.ZConect.Base
{
    class Fluxos : Page
    {
        public Fluxos(IWebDriver driver, Utils _utils)
        {
            Driver = driver;
            _Utils = _utils;
        }

        public Simulacao CotacaoDadosBasicos()
        {
            var homePage = new HomePage(Driver, _Utils);

            var cotacao = homePage.ClickFacaUmaCotacao();
            cotacao.SelectFeminineGender();
            cotacao.EnterBirthdate();
            cotacao.EnterProfession();
            cotacao.EnterMonthlyIncome();
            cotacao.EnterCellphone();
            cotacao.EnterEmail();
            cotacao.PurchaseButton();

            return new Simulacao(Driver, _Utils);

        }

    }
}
