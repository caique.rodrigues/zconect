﻿using OpenQA.Selenium;
using System.Windows.Forms;
using TestUtils;

namespace Zurich.ZConect.Models
{
    class SimulacaoPage : Page
    {
        public SimulacaoPage(IWebDriver driver, Utils _utils)
        {
            Driver = driver;
            _Utils = _utils;
        }

        public void WaitForLoadedPage() => _Utils.EsperarCarregar(Driver, By.XPath("//*[@class='splash-screen__box text-center']"));
        public CotacaoPage PurchaseButton()
        {
            _Utils.EsperaAparecerClica(Driver, By.XPath("//button[@id='purchaseButton']"));
            return new CotacaoPage(Driver, _Utils);
        }
        public void EnterValorBeneficio(string valorBeneficio = "2000000") => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@type='text']"), valorBeneficio);


        public CotacaoPage ClickBackwardButton()
        {
            _Utils.EsperaAparecerClica(Driver, By.XPath("//a[@class='btn-anchor']"));
            return new CotacaoPage(Driver, _Utils);
        }

        public void SelectPeriod(string period)
        {
            Driver.FindElement(By.XPath("//select[@class='select-input input select-periodicidade']")).Click();
            Driver.FindElement(By.XPath($"//option[@value='{period}']")).Click();
        }

        public void SelectTempoVigenciaCobertura() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[@for='30-anos']"));
        public void ClickNextButtonVigenciaCobertura() => Driver.FindElement(By.XPath("//button[@id='goto-next-step']")).Click();
        public void ClickButtonVitalicio() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[@for='vitalicio']"));
        public void SelectAntecipacaoIdade() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[@for='idade-70']"));
        public void SelectAntecipacaoTempo() => Driver.FindElement(By.Id("tempo")).Click();
        public void EnterAntecipacaoTempo() => Driver.FindElement(By.Id("prazo-20")).Click();
        public void GoToCapitalSegurado() => Driver.FindElement(By.Id("goto-capital-segurado")).Click();
        public void ClickPeriodoDecrescimo() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[@for='descrescimo-30']"));
        public void GoToAntecipacao() => _Utils.EsperaAparecerClica(Driver, By.XPath("//button[@id='goto-antecipacao']"));
        public void ChooseWholeLife() => _Utils.EsperaAparecerClica(Driver, By.XPath("//input[@id='morte']"));
        public void ChooseWholeLifeDecrescente() => _Utils.EsperaAparecerClica(Driver, By.XPath("//input[@id='morte-decrescente-2112']"));
        public void GoToAntecipacaoOrDecrescimo() => _Utils.EsperaAparecerClica(Driver, By.XPath("//button[@id='goto-decrescimo-ou-antecipacao']"));
        public string Preço => Driver.FindElement(By.XPath("//var[@class='amount__pay-real']")).Text;
        public bool PeriodIsSelected(string period) => Driver.FindElement(By.XPath($"//option[@value='{period}']")).Selected;
        public string TextoBotaoSuperior => Driver.FindElement(By.XPath("//section[@class='btn-section']//span[@class='valor-pagar-periodo']")).Text;
        public string TextoValorAPagar => Driver.FindElement(By.XPath("//span[@class='valor-pagar-periodo margin-top-16']")).Text;
        public string TextoValorAPagarInferior => Driver.FindElement(By.XPath("//div[@class='amount']//span[@class='valor-pagar-periodo']")).Text;
        public string TextoValorBotaoInferior => Driver.FindElement(By.XPath("//section[@class='section-footer-hire bg-urbanSky lg-d-block']//button[@class='button hire-btn forward-behavior contract-insurance']//span[@class='valor-pagar-periodo']")).Text;
        public string Produto => Driver.FindElement(By.XPath("//span[@class='produto-title']")).Text;
        public bool TempoVigencia => Driver.FindElement(By.XPath("//input[@value='30']")).Selected;
        public string CapitalSegurado => Driver.FindElement(By.XPath("//input[@name='capital-segurado']")).GetAttribute("value");
        public bool Decrescimo => Driver.FindElement(By.XPath("//input[@value='30']")).Selected;
        public bool AntecipacaoPagamento => Driver.FindElement(By.XPath("//input[@value='70']")).Selected;
        public bool IdadeAntecipacao => Driver.FindElement(By.XPath("//input[@value='20']")).Selected;
        public bool ExibicaoTempo => Driver.FindElement(By.XPath("//label[contains (text(),'20 ANOS')]")).Displayed;
        public bool TempoCobertura => Driver.FindElement(By.XPath("//input[@value='30' and @class='fast-selection__radio-field right-deadline option-prazo']")).Selected;
        public string PreçoBotaoSuperior => Driver.FindElement(By.XPath("//button[@class='cta-section__button button forward-behavior contract-insurance']")).Text;
        public string PreçoFaixaProduto => Driver.FindElement(By.XPath("//div[@class='produto-footer-currency']")).Text;
        public string TextoValorAPagarPreçoInferior => Driver.FindElement(By.XPath("//div[@class='amount__pay']//var[@class='amount__pay-real']")).Text;
        public string PreçoBotaoInferior()
        {
            string reaisInferior = Driver.FindElement(By.XPath("//span[@class='btn-default']//span[@class='contribution-real']")).Text;
            string centsinferior = Driver.FindElement(By.XPath("//span[@class='btn-default']//span[@class='contribution-cents']")).Text;
            string preçoBotaoInferior = reaisInferior + centsinferior;

            return preçoBotaoInferior;
        }
        public bool ProdutoTermLife => Driver.FindElement(By.XPath("//input[@name='product-2113']")).Selected;
        public bool ProdutoWholeLifeDecrescente => Driver.FindElement(By.XPath("//input[@name='product-2112']")).Selected;
        public bool ProdutoWholeLife => Driver.FindElement(By.XPath("//input[@name='product-2111']")).Selected;
        public void ClickBotaoContiuarDepois() => Driver.FindElement(By.XPath("//a[@class='botao-continuar-depois']")).Click();
        public string UrlContinuarDepois()
        {
            _Utils.EsperaSerClicavelClica(Driver, By.XPath("//div[@class='bt-link-text']"));
            string URL = Clipboard.GetText();

            return URL;
        }
        public bool Period(string period) => Driver.FindElement(By.XPath($"//option[@value='{period}']")).Selected;
        public string PeriodicidadeFaixa => Driver.FindElement(By.XPath("//div[@class='produto-footer-valor']//span[@class='valor-pagar-periodo']")).Text;
        public bool PeriodoDecrescimo => Driver.FindElement(By.XPath("//input[@value='30' and @alt='2112']")).Selected;
    }
}
