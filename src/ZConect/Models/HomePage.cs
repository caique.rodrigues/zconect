﻿
using OpenQA.Selenium;
using TestUtils;


namespace Zurich.ZConect.Models
{
    class HomePage : Page
    {
        public HomePage(IWebDriver driver, Utils _utils)
        {
            _Utils = _utils;
            Driver = driver;
        }

        public CotacaoPage ClickFacaUmaCotacao()
        {
            _Utils.EsperaAparecerClica(Driver, By.XPath("//span[contains(text(),'FAÇA UMA COTAÇÃO')]"));
            return new CotacaoPage(Driver, _Utils);
        }
    }
}
