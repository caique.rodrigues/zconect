﻿using OpenQA.Selenium;
using TestUtils;


namespace Zurich.ZConect.Models
{
    class CotacaoPage : Page
    {
        public CotacaoPage(IWebDriver driver, Utils _utils)
        {
            Driver = driver;
            _Utils = _utils;
        }

        public void EnterName(string name) => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='name']"), $"{name} Teste");
        public void EnterCpf(string CPF) => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='cpf']"), CPF);
        public void EnterBirthdate(string Birthdate = "14/04/1990") => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='birthdate']"), Birthdate);
        public void EnterMonthlyIncome(string MonthlyIncome = "10000") => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='monthly-income']"), MonthlyIncome);
        public void EnterProfession(string Profession = "adv")
        {
            _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='profession']"), Profession);
            _Utils.EsperaAparecerClica(Driver, By.XPath("//div[@class='autocomplete active']//div[1]"));
        }
        public void SelectFeminineGender() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[contains(text(),'Feminino')]"));
        public void SelectMasculineGender() => _Utils.EsperaAparecerClica(Driver, By.XPath("//label[contains(text(),'Masculino')]"));
        public void EnterEmail(string email = "tst@tst.com.br") => _Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='email']"), email);
        public void EnterCellphone(string cellphone = "21985693204") =>_Utils.EsperaAparecerClicaEscreve(Driver, By.XPath("//input[@id='cellphone']"), cellphone);
        public SimulacaoPage PurchaseButton() 
        { 
            _Utils.EsperaAparecerClica(Driver, By.XPath("//button[@id='purchaseButton']"));
            return new SimulacaoPage(Driver, _Utils);
        }

        public string Nome => _Utils.GetElement(Driver, By.XPath("//input[@type='text' and @id='name']")).GetAttribute("value");
        public bool SexoMasculino => Driver.FindElement(By.Id("sex-male")).Selected;
        public bool SexoFeminino => Driver.FindElement(By.Id("sex-female")).Selected;
        public string DataNascimento => _Utils.GetElement(Driver, By.XPath("//input[@type='text' and @id='birthdate']")).GetAttribute("value");
        public string Cpf => _Utils.GetElement(Driver, By.XPath("//input[@type='text' and @id='cpf']")).GetAttribute("value");
        public string Renda => _Utils.GetElement(Driver, By.XPath("//input[@type='text' and @id='monthly-income']")).GetAttribute("value");
        public string Profissao => _Utils.GetElement(Driver, By.XPath("//input[@type='text' and @id='profession']")).GetAttribute("value");
        public string Email => _Utils.GetElement(Driver, By.XPath("//input[@type='email' and @id='email']")).GetAttribute("value");
        public string Celular => _Utils.GetElement(Driver, By.XPath("//input[@type='tel' and @id='cellphone']")).GetAttribute("value");
        
    }
}
