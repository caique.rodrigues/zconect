﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Text;
using TestUtils;
using Zurich.ZConect.Base;

namespace Zurich.ZConect
{
    [TestFixture]
    class ZConectCotacao
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;

        Utils _utils;
        private IConfiguration config;
        Fluxos fluxos;

        [SetUp]

        public void SetupTest()
        {
            _utils = new Utils();
            driver = new ChromeDriver();
            config = new ConfigurationBuilder().AddJsonFile("Config/config.json").Build();
            baseURL = config["baseUrlUAT"];
            verificationErrors = new StringBuilder();
            fluxos = new Fluxos(driver, _utils);
        }

        [TearDown]
        public void TeardownTest()


        {
            try
            {
                if (driver != null)
                    driver.Quit();

                if (driver != null)
                    driver.Close();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void FluxoCompleto()
        {
            _utils.IrEMaximizar(driver, baseURL);


            //fluxos.CotacaoDadosBasicos();





        }
    }
}
