﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace TestUtils
{

    public class Utils
    {
        public void ClickClearSendKeys(IWebDriver driver, By by, string text)
        {
            driver.FindElement(by).Click();
            driver.FindElement(by).Clear();
            driver.FindElement(by).SendKeys(text);
        }
        public void ClickClear(IWebDriver driver, By by)
        {
            driver.FindElement(by).Click();
            driver.FindElement(by).Clear();
            
        }
        public void IrEMaximizar(IWebDriver driver, string url)
        {
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
        }

        public dynamic MassTest(string fileName)
        {
            var dir = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "..\\..\\..\\DataSource\\" + fileName + "");
            var json = File.ReadAllText(dir);
            return JObject.Parse(json);

        }

        public string TabPegaNomeAtual(IWebDriver driver)
        {
            return driver.CurrentWindowHandle;
        }

        public string TabAbertaPegaNome(IWebDriver driver)
        {
            driver.ExecuteJavaScript("window.open();");
            System.Threading.Thread.Sleep(2000);
            return driver.WindowHandles.Last();
        }

        public void TabFechaAtual(IWebDriver driver)
        {
            driver.ExecuteJavaScript("window.close();");
        }

        public ReadOnlyCollection<string> TabPegaTodosNomes(IWebDriver driver)
        {
            return driver.WindowHandles;
        }

        public void TabMudarPara(IWebDriver driver, string nameWindows)
        {
            driver.SwitchTo().Window(nameWindows);
        }

        public virtual void CheckboxButton(IWebDriver driver, By by)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(driver.FindElement(by)).Click().Build().Perform();
        }

        public virtual void AguardarTotalCarregamento(IWebDriver driver, int tempo = 30)
        {

            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));

            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

        }

        public virtual void RadioButton(IWebDriver driver, IWebElement element)
        {
            element.Click();
        }

        public virtual IWebElement EsperaAparecerClicaEscreve(IWebDriver driver, By by, string text, int tempo = 100)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(by));
            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed && element.TagName != null)
            {
                element.Click();
                element.SendKeys(text);
            }
            return null;
        }

        public virtual IWebElement EsperaAparecerClica(IWebDriver driver, By by, int tempo = 40)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(by));
            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed && element.TagName != null)
            {
                element.Click();
            }
            return element;
        }

        public virtual IWebElement GetElement(IWebDriver driver, By by, int tempo = 40)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(by));
            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed && element.TagName != null)
            {
                return element;
            }
            return null;
            
        }


        public virtual IWebElement EsperaElementoAparecer(IWebDriver driver, By by, int tempo = 60)
        {

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed)
            {
                return element;
            }
            return null;

        }

        public virtual bool EsperarCarregar(IWebDriver driver, By by, int tempo = 120)
        {

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            wait.Timeout = TimeSpan.FromSeconds(45);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(by));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            wait.Timeout = TimeSpan.FromSeconds(tempo);
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(by)))
            {
                return true;
            }
            else
                return false;
        }

        public virtual void EventFire(IWebDriver driver, string elemento, string evento)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("function eventFire(el, etype){ if (el.fireEvent) { el.fireEvent('on' + etype); } else { var evObj = document.createEvent('Events'); evObj.initEvent(etype, true, false); el.dispatchEvent(evObj); } }; window.onload=eventFire(" + elemento + ",'" + evento + "')");
            js.ExecuteScript(stringBuilder.ToString());
        }

        public virtual IWebElement EsperaSerClicavelClica(IWebDriver driver, By by, int tempo = 40)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tempo));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(by));

            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(by)).Displayed && element.TagName != null)
            {
                element.Click();
            }
            return null;
        }
    }
}


